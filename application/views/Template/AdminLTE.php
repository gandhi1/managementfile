<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $this->session->userdata('nama_user'); ?> | <?php echo $this->uri->segment(1,0); ?></title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="<?php echo base_url("adminLTE/"); ?>bower_components/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo base_url("adminLTE/"); ?>bower_components/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?php echo base_url("adminLTE/"); ?>bower_components/Ionicons/css/ionicons.min.css">
  <link rel="stylesheet" href="<?php echo base_url("adminLTE/"); ?>bower_components/select2/dist/css/select2.min.css">
  <link rel="stylesheet" href="<?php echo base_url("adminLTE/"); ?>dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="<?php echo base_url("adminLTE/"); ?>dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="<?php echo base_url("adminLTE/"); ?>bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo base_url("adminLTE/"); ?>bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <link rel="stylesheet" href="<?php echo base_url("adminLTE/"); ?>bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <link rel="stylesheet" href="<?php echo base_url("adminLTE/"); ?>plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="<?php echo base_url(); ?>" class="logo">
          <span class="logo-mini"><b>F</b>m</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Files</b> Manager</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
<!--       <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a> -->
      <?php echo $navbarCustomMenu; ?>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <?php echo $userPanel; ?>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <?php echo $menus; ?>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <?php echo $breadcumb; ?>
    <!-- Main content -->
    <section class="content">
      <?php echo $contents; ?>

      <!-- modal password -->
      <div class="modal fade" id="modal-password">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Change Password</h4>
            </div>
            <form method="POST" action="<?php echo base_url("index.php/Welcome/ubahPassword"); ?>">
            <div class="modal-body">
              <div class="row">
                <div class="col-sm-12">
                  <div class="form-group">
                    <label for="exampleInputEmail1">New Password</label>
                    <input type="password" class="form-control" id="exampleInputEmail1"  name="nPassword">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Repeat Password</label>
                    <input type="password" class="form-control" id="exampleInputEmail1"  name="rPassword">
                  </div>                  
                </div>
                <div class="col-sm-12">
                  <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                  <input type="submit" name="tambah" class="btn btn-primary pull-right" value="Save"/>
                </div>
              </div>
            </div>
            </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>

        <!-- modal profile -->
      <div class="modal fade" id="modal-profil">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Change Profile</h4>
            </div>
            <form method="POST" action="<?php echo base_url("index.php/Welcome/ubahUsername"); ?>">
            <div class="modal-body">
              <div class="row">
                <div class="col-sm-12">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Username</label>
                    <input type="text" class="form-control" id="exampleInputEmail1"  name="username" value="<?php echo $this->session->userdata("nama_user"); ?>">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">User Login</label>
                    <input type="text" class="form-control" id="exampleInputEmail1"  name="userlogin" value="<?php echo $this->session->userdata("login_user"); ?>">
                  </div> 
                </div>
                <div class="col-sm-12">
                  <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                  <input type="submit" name="tambah" class="btn btn-primary pull-right" value="Save"/>
                </div>
              </div>
            </div>
            </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>     
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 0.0.1
    </div>
    <strong>Copyright &copy; 2014-2018 </strong> 
  </footer>
  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->
<script src="<?php echo base_url("adminLTE/"); ?>bower_components/jquery/dist/jquery.min.js"></script>
<script src="<?php echo base_url("adminLTE/"); ?>bower_components/jquery-ui/jquery-ui.min.js"></script>
<script src="<?php echo base_url("adminLTE/"); ?>bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="<?php echo base_url("adminLTE/"); ?>bower_components/select2/dist/js/select2.full.min.js"></script>
<script src="<?php echo base_url("adminLTE/"); ?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url("adminLTE/"); ?>bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<script src="<?php echo base_url("adminLTE/"); ?>bower_components/raphael/raphael.min.js"></script>
<script src="<?php echo base_url("adminLTE/"); ?>bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<script src="<?php echo base_url("adminLTE/"); ?>bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<script src="<?php echo base_url("adminLTE/"); ?>bower_components/moment/min/moment.min.js"></script>
<script src="<?php echo base_url("adminLTE/"); ?>bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="<?php echo base_url("adminLTE/"); ?>bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="<?php echo base_url("adminLTE/"); ?>plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<script src="<?php echo base_url("adminLTE/"); ?>bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url("adminLTE/"); ?>bower_components/fastclick/lib/fastclick.js"></script>
<script src="<?php echo base_url("adminLTE/"); ?>dist/js/adminlte.min.js"></script>
<!-- page script -->
<script>
  $(function () {
    $('#table1').DataTable()
    $('.select2').select2(
      {
          tags: true,
          tokenSeparators: [',', ' ']
      })
    $('#reservation').daterangepicker()
  })
</script>
</body>
</html>
