      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo base_url("adminLTE/"); ?>dist/img/iss.png" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo $this->session->userdata("nama_user"); ?></span>
            </a>
            <ul class="dropdown-menu">
              <li class="user-footer">
                <a href="#" data-toggle="modal" data-target="#modal-profil">
                  <span class="fa fa-user"> Change Profile</span>
                </a>
              </li>
              <li class="user-footer">
                <a href="#" data-toggle="modal" data-target="#modal-password">
                  <span class="fa fa-key"> Change Password</span>
                </a>
              </li>
              <li class="user-footer">
                <a href="<?php echo base_url("index.php/Welcome/Users/"); ?>" >
                  <span class="fa fa-users"> Users</span>
                </a>
              </li>              
            </ul>
          </li>
          <li class="dropdown user user-menu">
            <a href="<?php echo base_url("index.php/Welcome/logout"); ?>" class="dropdown-toggle">
              <span class="fa  fa-power-off"> Sign Out</span>
            </a>
          </li>
          <!-- Control Sidebar Toggle Button -->
        </ul>
      </div>