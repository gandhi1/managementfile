<ul class="sidebar-menu" data-widget="tree">
  <form action="<?php echo base_url("index.php/Welcome/Dashboard/createFolder") ?>" method="POST" class="sidebar-form">
    <div class="input-group">
      <input type="text" name="q" class="form-control" placeholder="New folder ">
      <span class="input-group-btn">
            <button type="submit" name="create" value="Y" id="create-btn" class="btn btn-flat"><i class="fa  fa-plus-square-o"></i>
            </button>
          </span>
    </div>
  </form>  
  <li class="header">Folder</li>
  <?php
  foreach ($menus as $key => $value) {
    if ($this->session->userdata('folder_id')==$value->id) { ?>
        <li class="treeview active">
          <a href="#">
            <i class="fa fa-folder-open-o"></i>
            <span><?php echo $value->nama_folder; ?></span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>            
          </a>
          <ul class="treeview-menu">
            <form action="<?php echo base_url("index.php/Welcome/Dashboard/renameFolder") ?>" method="POST" class="sidebar-form">
              <div class="input-group">
                <input type="hidden" name="id" value="<?php echo $value->id; ?>">
                <input type="text" name="q" class="form-control" value="<?php echo $value->nama_folder; ?>">
                <span class="input-group-btn">
                      <button type="submit" name="create" value="Y" id="create-btn" class="btn btn-flat"><i class="fa  fa-edit"></i>
                      </button>
                    </span>
              </div>
            </form>
          </ul>
        </li>
      </li>
    <?php }
    else{ ?>
      <li >
        <a href="<?php echo base_url("index.php/Welcome/setSession/".$value->id."/".$value->nama_folder); ?>">
          <i class="fa fa-folder"></i> <span> <?php echo $value->nama_folder; ?></span>
        </a>
      </li>

    <?php } 
  }?>

</ul>