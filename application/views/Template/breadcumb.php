<section class="content-header">
  <h1>
    <?php echo str_replace("_", " ",$this->uri->segment(2,0)); ?>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url("index.php/"); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active"><?php echo str_replace("_", " ",$this->uri->segment(1,0)); ?></li>
  </ol>
</section>