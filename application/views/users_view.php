<div class="row">
	<div class="col-xs-12">
	  <!-- /.box -->
	  <div class="box box-warning">
	    <div class="box-header">
	    	<?php if ($this->session->userdata('role')=="admin") { ?>
			      <button type="button" class="btn btn-flat btn-success" data-toggle="modal" data-target="#modal-new-user">
			        Add New User
			      </button>
	    	<?php } ?>
	    </div>
	    <!-- /.box-header -->
	    <div class="box-body">
	      <table id="table1" class="table table-bordered table-striped">
	        <thead>
	        <tr>
	          <th width="10">No</th>
	          <th>User Name</th>
	          <th>User Login</th>
	          <th>Role</th>
	          <th width="10">Option</th>
	        </tr>
	        </thead>
	        <tbody>                                	
	        	<?php 
	        	$no = 1;
	        	foreach ($users->result() as $key => $value) { ?>
	        		<tr>
	        			<td><?php echo $no++; ?></td>
	        			<td><?php echo $value->nama; ?></td>
	        			<td><?php echo $value->user_login; ?></td>
	        			<td><?php echo $value->role; ?></td>
	        			<td>
	        				<?php if ($this->session->userdata('role')=="admin") { 
	        					if ($this->session->userdata('kode_user') != $value->id) { ?>
							<button type="button" class="btn btn-flat btn-sm btn-danger" data-toggle="modal" data-target="#modal-delete-<?php echo $value->id; ?>">
								<span class="fa  fa-trash"></span>
							</button>		                  	
		                  	<div class="modal modal-danger fade" id="modal-delete-<?php echo $value->id; ?>">
		                  		<div class="modal-dialog">
		                  			<div class="modal-content">
		                  				<div class="modal-header">
									        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
									        <span aria-hidden="true">&times;</span></button>
									        <h4 class="modal-title">Delete File </h4>
		                  				</div>
		                  				<div class="modal-body">
		                  					<h4>
		                  					Do you want to delete user <b><?php echo $value->nama; ?></b> ?
		                  					</h4>
		                  				</div>
		                  				<div class="modal-footer">
									        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
						                  	<form action="<?php echo base_url("index.php/Welcome/Dashboard/deleteUser/"); ?>" method="POST">
						                  		<input type="hidden" name="id" value="<?php echo $value->id; ?>">
						                  		<button type="submit" value="<?php echo $value->id; ?>" class="btn btn-flat btn-success" name="delete"> Yes !
						                  		</button>
						                  	</form>											
		                  				</div>
		                  			</div>
		                  		</div>
		                  	</div>
							<?php } } ?>
	        			</td>
	        		</tr>
	        	<?php } ?>
	        </tbody>
	      </table>
	    </div>
	    <!-- /.box-body -->
	  </div>
	  <!-- /.box -->
	</div>
<!-- /.col -->
</div>

<div class="modal fade" id="modal-new-user">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Add New User</h4>
      </div>
      <div class="modal-body">
      	<div class="row">
			<?php echo form_open_multipart('Welcome/Dashboard/tambahUser',array('class'=>'form-horizontal'));?>
      		<div class="col-sm-12">
			<div class="form-group">
				<label class="col-sm-2 control-label">Role</label>
				<div class="col-sm-10">
					<select class="form-control" name="role">
						<option value="admin">Admin</option>
						<option value="user">User</option>
					</select>
				</div>
			</div>      			
			<div class="form-group">
				<label class="col-sm-2 control-label">User Name</label>
				<div class="col-sm-10">
					<input type="text" name="user_name" class="form-control" >
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">User login</label>
				<div class="col-sm-10">
					<input type="text" name="user_login" class="form-control" >
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Password</label>
				<div class="col-sm-10">
					<input type="password" name="user_password" class="form-control" >
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Repeat Password</label>
				<div class="col-sm-10">
					<input type="password" name="user_rpassword" class="form-control" >
				</div>
			</div>			
      		</div>
      		<div class="col-sm-12">
				<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
				<input type="submit" name="tambah" class="btn btn-primary pull-right" value="Save"/>
      		</div>
			</form>
      	</div>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->