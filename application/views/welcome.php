      <div class="row">
        <div class="col-xs-12">
          <!-- /.box -->
          <div class="box box-primary">
            <div class="box-header">
              <button type="button" class="btn btn-flat btn-success" data-toggle="modal" data-target="#modal-default">
                Add New File
              </button>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
			  <br>
              <table id="table1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th width="10">No</th>
                  <th>File Name</th>
                  <th>Description</th>
                  <th>Tag</th>
                  <th width="10">Edit</th>
                  <th width="10">Active</th>
                  <th width="10">Action</th>
                </tr>
                </thead>
                <tbody>                                	
                	<?php 
                	$no = 1;
                	foreach ($files as $key => $value) { ?>
		                <tr>
		                  <td><?php echo $no++; ?></td>
		                  <td>
		                  	<?php echo "<b>".$value->nama_file."</b>  ( ".$value->size_file." kB)"; ?>
		                  </td>
		                  <td>
		                  	<?php echo $value->desc; ?>
		                  </td>
		                  <td>
					      	<?php 
					      	$echo = "";
					      	if (count(explode(",", $value->tag))>1 || $value->tag != "") {
					      		$idT = "";
					      		foreach (explode(",", $value->tag) as $key => $vt) {
					      			$idT = $idT."'{$vt}',";
					      		}
					      		
								foreach ($this->Model_tags->getId(rtrim($idT,","))->result() as $key => $values) {
									$echo = $echo.$values->tag.",";
								}
								echo rtrim($echo,",");
					      	}
					      	?>
		                  </td>
		                  <td>
							<button type="button" class="btn btn-flat btn-sm btn-warning" data-toggle="modal" data-target="#modal-info-<?php echo $value->id; ?>">
								<span class="fa  fa-edit"></span>
							</button>
		                  </td>
		                  <td>
		                  	<?php
		                  	if ($value->active==1) { ?>
		                  		<a class="btn btn-flat btn-sm btn-success" 
		                  		href="<?php echo base_url("index.php/Welcome/active/".$value->id."/0") ?>"
		                  		>
		                  			<span class="fa  fa-check-square-o"></span>
		                  		</a>
		                  	<?php }
		                  	else{ ?>
		                  		<a class="btn btn-flat btn-sm btn-default" 
		                  		href="<?php echo base_url("index.php/Welcome/active/".$value->id."/1") ?>">
		                  			<span class="fa fa-circle-o"></span>
		                  		</a>
		                  	<?php } ?>		
		                  </td>
		                  <td>
							<button type="button" class="btn btn-flat btn-sm btn-danger" data-toggle="modal" data-target="#modal-delete-<?php echo $value->id; ?>">
								<span class="fa  fa-trash"></span>
							</button>		                  	
		                  	<div class="modal modal-danger fade" id="modal-delete-<?php echo $value->id; ?>">
		                  		<div class="modal-dialog">
		                  			<div class="modal-content">
		                  				<div class="modal-header">
									        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
									        <span aria-hidden="true">&times;</span></button>
									        <h4 class="modal-title">Delete File </h4>
		                  				</div>
		                  				<div class="modal-body">
		                  					<h4>
		                  					Are you sure to delete file <b><?php echo $value->nama_file; ?></b> ?
		                  					</h4>
		                  				</div>
		                  				<div class="modal-footer">
									        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
											<form action="<?php echo base_url("index.php/Welcome/Dashboard/deleteFile/"); ?>" method="POST">
												<input type="hidden" name="ext" value="<?php echo $value->type_file; ?>">
												<input type="hidden" name="name" value="<?php echo $value->nama_file ?>">
												<button type="submit" value="<?php echo $value->id; ?>" class="btn btn-success" name="delete">Yes !</button>
											</form>
		                  				</div>
		                  			</div>
		                  		</div>
		                  	</div>
		                  </td>
		                </tr>
						<div class="modal fade" id="modal-info-<?php echo $value->id; ?>">
						  <div class="modal-dialog">
						    <div class="modal-content">
						      <div class="modal-header">
						        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
						          <span aria-hidden="true">&times;</span></button>
						        <h4 class="modal-title">Rename File </h4>
						      </div>
						      <form method="POST" action="<?php echo base_url("index.php/Welcome/Rename"); ?>">
						      	<input type="hidden" name="id" value="<?php echo $value->id; ?>">
						      	<input type="hidden" name="ext" value="<?php echo $value->type_file; ?>">
						      	<input type="hidden" name="on" value="<?php echo $value->nama_file; ?>">
							      <div class="modal-body">
							        <div class="form-group">
							          <label for="exampleInputEmail1">File name</label>
							          <input type="text" class="form-control" id="exampleInputEmail1" 
							          value="<?php echo $value->nama_file; ?>" name="nama_file">
							        </div>
							        <div class="form-group">
							          <label for="exampleInputEmail1">Descripsi</label>
							          <textarea id="exampleInputEmail1" class="form-control" name="desc"><?php echo $value->desc; ?></textarea>

							        </div>
							        <div class="form-group">
							          <label for="exampleInputEmail1">Tags</label>
						                <select class="form-control select2" multiple="multiple" data-placeholder="Select a State" name="tags[]" 
						                        style="width: 100%;">
						                        <?php
										      	$echo = array();
										      	if (count(explode(",", $value->tag))>1 || $value->tag != "") {
										      		$idT = "";
										      		foreach (explode(",", $value->tag) as $key => $vt) {
										      			$idT = $idT."'{$vt}',";
										      		}
													foreach ($this->Model_tags->getId(rtrim($idT,","))->result() as $key => $values) {
														array_push($echo, $values->tag);
													}
													// kalau datanya ada, tampilkan selectnya
													foreach ($tags->result() as $key => $vl) {
														echo "<option ";
														foreach ($echo as $v) {
															if ($vl->tag == $v) {
																echo "selected";
															}
														}
														echo " >{$vl->tag}</option>";
													}
										      	}
										      	else{
										      		// kalau defaultnya kosong.
													foreach ($tags->result() as $key => $vl) {
														echo "<option>{$vl->tag}</option>";
													}
										      	}
						                        ?>
						                </select>
							        </div>
							      </div>
							      <div class="modal-footer">      	
							        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
							        <button type="submit" class="btn btn-primary">Save</button>
							      </div>
						      </form>
						    </div>
						  </div>
						</div>		                
                	<?php } ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
        <div class="modal fade" id="modal-default">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add New File</h4>
              </div>
              <div class="modal-body">
              	<div class="row">
						<?php echo form_open_multipart('Welcome/Dashboard/tambahFile',array('class'=>'form-horizontal'));?>
              		<div class="col-sm-12">
					<div class="form-group">
						<label for="inputEmail3" class="col-sm-2 control-label">File</label>
						<div class="col-sm-10">
							<input type="file" name="form_name" class="form-control" >
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Description</label>
						<div class="col-sm-10">
							<textarea class="form-control" name="desc"></textarea>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Tag</label>
						<div class="col-sm-10">
			                <select class="form-control select2" multiple="multiple" data-placeholder="Select a State" name="tags[]" 
			                        style="width: 100%;">
			                        <?php
			                        foreach ($tags->result() as $key => $value) {
			                        	echo "<option>{$value->tag}</option>";
			                        }
			                        ?>
			                </select>
						</div>
					</div>
              		</div>
              		<div class="col-sm-12">
						<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
						<input type="submit" name="tambah" class="btn btn-primary pull-right" value="Save"/>
              		</div>
						</form>
              	</div>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->