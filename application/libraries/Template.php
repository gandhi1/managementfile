<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Template {

	function show($view='',$contents=array(),$breadcumb=array(),$menus=array(),$userPanel=array(),$navbarCustomMenu=array()){
		$this->CI =& get_instance();
    	$data['contents'] = $this->CI->load->view($view,$contents,TRUE);
    	$data['breadcumb'] = $this->CI->load->view("Template/breadcumb",$breadcumb,TRUE);
    	$data['menus'] = $this->CI->load->view("Template/menus",$menus,TRUE);
		$data['userPanel'] = "";
//		$data['userPanel'] = $this->CI->load->view("Template/userPanel",$userPanel,TRUE);		
    	$data['navbarCustomMenu'] = $this->CI->load->view("Template/navbarCustomMenu",$navbarCustomMenu,TRUE);	
    	return $this->CI->load->view("Template/AdminLTE", $data,FALSE);
	}
}