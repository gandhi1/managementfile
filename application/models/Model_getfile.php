<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Model_getfile extends CI_Model {

	function getAll(){
		// just get data where view and active is 1
		$q = "SELECT * FROM files Fi, folder Fo
				WHERE Fi.active =1 
				AND Fi.id_folder=Fo.id";
		$e = $this->db->query($q);
		return $e;
	}

	function cari($keyword){
		$q = "SELECT * FROM files Fi, folder Fo WHERE 
		(
		Fi.nama_file LIKE '%{$keyword}%' 
		OR Fi.desc LIKE '%{$keyword}%' 
		OR Fi.tag LIKE (SELECT CONCAT('%',GROUP_CONCAT(id),'%') FROM tags WHERE tag LIKE '%{$keyword}%')) 
		AND Fi.active =1 AND Fi.id_folder=Fo.id ";
		$e = $this->db->query($q);
		return $e;		
	}

}