<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Model_users extends CI_Model {

	private $tablename = "users";

	function getAll(){
		$q = "SELECT id,nama,user_login,role FROM users";
		$e = $this->db->query($q);
		return $e;		
	}

	function newUser($data){
		return $this->db->insert($this->tablename,$data);
	}

	function deleteUser($id){
		$this->db->where('id', $id);
		return $this->db->delete($this->tablename);
	}
}