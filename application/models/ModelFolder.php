<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class ModelFolder extends CI_Model {

	private $tablename = "folder";

	function insert($data){
		return $this->db->insert($this->tablename,$data);
	}

	function delete($id){
		$this->db->where('id', $id);
		return $this->db->delete($this->tablename);
	}

	function rename($id,$name){
		$this->db->set('nama_folder', $name);
		$this->db->where('id', $id);
		return $this->db->update($this->tablename);
	}

	function getAll(){
		return $this->db->get($this->tablename)->result();
	}

	function getFirst(){
		return $this->db->get($this->tablename,1)->row();
	}

}