<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class ModelFile extends CI_Model {
	
	private $tablename = "files";

	function insert($data){
		return $this->db->insert($this->tablename,$data);
	}

	function delete($id){
		$this->db->where('id', $id);
		return $this->db->delete($this->tablename);
	}

	function rename($id,$name,$desc,$tag){
		$data = array('nama_file'=>$name,'desc'=>$desc,'tag'=>$tag);
		$this->db->set($data);
		$this->db->where('id', $id);
		return $this->db->update($this->tablename);
	}

	function active($id,$value){
		$this->db->set('active', $value);
		$this->db->where('id', $id);
		return $this->db->update($this->tablename);
	}

	function getBy($id){
		return $this->db->query("SELECT * FROM files WHERE id_folder='{$id}'")->result();
	}

}