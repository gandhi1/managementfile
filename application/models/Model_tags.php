<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Model_tags extends CI_Model {


	function getAll(){
		$q = "SELECT * FROM tags";
		$e = $this->db->query($q);
		return $e;
	}

	function getIn($In){
		if ($In != "") {
			$q = "SELECT * FROM tags WHERE tag IN ({$In})";
			$e = $this->db->query($q);
			return $e;
		}
		else{
			$q = "SELECT * FROM tags WHERE id = ''";
			$e = $this->db->query($q);
			return $e;

		}
	}
	function getId($Id){
		$q = "SELECT * FROM tags WHERE id IN ({$Id})";
		$e = $this->db->query($q);
		return $e;
	}
	function save($tag){
		$q = "INSERT INTO tags (tag)
				SELECT * FROM (SELECT '{$tag}') AS tmp
				WHERE NOT EXISTS (
				SELECT tag FROM tags WHERE tag = '{$tag}'
				) LIMIT 1;";
		$e = $this->db->query($q);
		return $e;
	}
}