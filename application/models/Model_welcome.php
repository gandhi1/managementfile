<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_welcome extends CI_Model {

	function cekUser($u,$p){
		$e = $this->db->get_where('users',array("user_login"=>$u,"password"=>$p));
		if (count($e->row()) == 1) {
			// kalau datanya hanya satu.
			return $e->row();
		}
		else{
			// kalau tidak ada data yang sesuai.
			return array();
		}
	}

	function ubahPassword($p){
		$this->db->set('password', $p);
		$this->db->where('id', $this->session->userdata("kode_user"));
		return $this->db->update("users");
	}
	function ubahUsername($value){
		$this->db->set($value);
		$this->db->where('id', $this->session->userdata("kode_user"));
		return $this->db->update("users");		
	}
}