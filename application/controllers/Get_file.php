<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH.'libraries/REST_Controller.php';

class Get_file extends REST_Controller{
	public function __construct(){
        parent::__construct();
        $this->load->model(array("Model_getfile"));
    }

	function index_get(){
		$ret = array(
			"message"=>"suksess",
			"url"=>base_url('files'),
			"data"=>$this->Model_getfile->getAll()->result()
		);
		$this->response($ret);
	}
}
?>