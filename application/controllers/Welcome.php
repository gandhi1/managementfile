<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function __construct(){
        parent::__construct();
        if (count($this->session->userdata()) == 1 ||count($this->session->userdata()) < 1) {
        	// kalo belum login
        	$this->load->view("Template/FormLogin");
        }
        $this->load->model(
        	array(
        		"ModelFile",
        		"ModelFolder",
        		"Model_welcome",
        		"Model_tags",
        		"Model_users"
        	)
        );
    }

	public function index(){
		if (count($this->session->userdata()) > 1) {
			redirect(base_url("index.php/Welcome/Dashboard"));
		}
	}

	public function Users(){
		if (count($this->session->userdata()) > 1) {
			$data['users'] = $this->Model_users->getAll();
			$data['menus'] =  $this->ModelFolder->getAll();
			$this->template->show('users_view',$data);
		}
		else{
        	redirect(base_url());			
		}		
	}

	public function Dashboard($d = NULL){
		if (count($this->session->userdata()) > 1) {
			if ($d == NULL) {
				$data['menus'] = $this->ModelFolder->getAll();
				$data['files'] = $this->ModelFile->getBy($this->session->userdata('folder_id'));
				$data['tags'] = $this->Model_tags->getAll();
				$this->template->show('welcome',$data);
			}
			else if ($d=="createFolder") {
				if ($this->input->post('create')) {
					// ambil valuenya
					$this->createFolderAct(str_replace(" ", "_", $this->input->post("q")));
					redirect(base_url("index.php/Welcome/Dashboard"));
				}
				else{
					redirect(base_url("index.php/Welcome/Dashboard"));
				}
			}
			else if ($d=="renameFolder") {
				if ($this->input->post('create')) {
					// ambil valuenya
					$this->renameFolderAct($this->session->userdata('folder_id'),$this->session->userdata('folder_name'),str_replace(" ", "_", $this->input->post("q")));
					$userdata = array(
						"folder_name"=>str_replace(" ", "_", $this->input->post("q"))
					);
					$this->session->set_userdata($userdata);					
					redirect(base_url("index.php/Welcome/Dashboard"));
				}
				else{
					redirect(base_url("index.php/Welcome/Dashboard"));
				}
			}
			else if ($d=="tambahFile") {
				if ($this->input->post("tambah")) {
					$this->uploadFileAct($this->session->userdata("folder_id"),$this->session->userdata("folder_name"));
					redirect(base_url("index.php/Welcome/Dashboard"));
				}
				else{
					redirect(base_url("index.php/Welcome/Dashboard"));
				}
			}
			else if ($d=="deleteFile") {
				if ($this->input->post("delete")) {
					$f = $this->session->userdata("folder_name")."/".$this->input->post("name").$this->input->post("ext");
					$this->deleteFileAct($this->input->post("delete"),$f);
					redirect(base_url("index.php/Welcome/Dashboard"));
				}
				else{
					redirect(base_url("index.php/Welcome/Dashboard"));
				}
			}
			else if ($d=="tambahUser") {
				if ($this->input->post("tambah")) {
					$data = array(
						'role'=>$this->input->post('role'),
						'nama'=>$this->input->post('user_name'),
						'user_login'=>$this->input->post('user_login'),
						'password'=>md5($this->input->post('user_rpassword'))
					);
					$this->Model_users->newUser($data);
					redirect(base_url("index.php/Welcome/Users"));
				}
				else{
					redirect(base_url("index.php/Welcome/Users"));
				}
			}
			else if ($d=="deleteUser") {
				if ($this->input->post("delete")) {
					$this->Model_users->deleteUser($this->input->post('id'));
					redirect(base_url("index.php/Welcome/Users"));
				}
				else{
					redirect(base_url("index.php/Welcome/Users"));
				}
			}
		}
		else{
        	redirect(base_url());			
		}
	}


	public function login(){
		if ($this->input->post('login')) {// jika tombol login di klik.
			$username = $this->input->post("username");
			$password = $this->input->post("password");
			$data = $this->Model_welcome->cekUser($username,md5($password));
			if (count($data)>0) {// kalau hasilnya ada datanya
				// select folder dlu.
				// lalu simpan juga idnya, dan nama foldernya di session sekalian.
				$folder = $this->ModelFolder->getFirst();
				$userdata = array(
					"kode_user"=>$data->id,
					"nama_user"=>$data->nama,
					"login_user"=>$data->user_login,
					"folder_id"=>$folder->id,
					"folder_name"=>$folder->nama_folder,
					"role"=>$data->role
				);
				$this->session->set_userdata($userdata);
				redirect(base_url());
			}
			else{
				redirect(base_url());
			}
		}
		else{
				redirect(base_url());
		}		
	}
	public function setSession($id,$value){
		if (count($this->session->userdata()) > 1) {
			$userdata = array(
				"folder_id"=>$id,
				"folder_name"=>$value
			);
			$this->session->set_userdata($userdata);
			redirect(base_url());
		}

	}
	public function logout(){
		$this->session->sess_destroy();
		redirect(base_url());
	}

	private function renameFolder($o,$name){ // done
		if (is_dir('files/'.$o)) {
			// kalau folder ada
			if(rename('./files/'.$o,'./files/'.$name)){
				// kalau folder terhapus
				return TRUE;
			}
			else{
				// gagal menghapus folder
				return FALSE;
			}
		}
		else{
			// folder belum ada
			return FALSE;
		}		
	}

	private function renameFolderAct($id,$o,$name){ // done
		if ($this->renameFolder($o,$name)) {
			if ($this->ModelFolder->rename($id,$name)) {
				return TRUE;
			}
			else{
				return FALSE;
			}
		}
		else{
			return FALSE;
		}
	}


	private function deleteFolder($name){ // done
		if (is_dir('files/'.$name)) {
			// kalau folder ada
			if(rmdir('./files/'.$name)){
				// kalau folder terhapus
				return TRUE;
			}
			else{
				// gagal menghapus folder
				return FALSE;
			}
		}
		else{
			// folder belum ada
			return FALSE;
		}
	}

	private function deleteFolderAct($id,$name){ // done
		if ($this->deleteFolder($name)) {
			if ($this->ModelFolder->delete($id)) {
				return TRUE;
			}
			else{
				return FALSE;
			}
		}
		else{
			return FALSE;
		}
	}

	private function createFolder($name){ // done
		if (!is_dir('files/'.$name)) {
			// kalau folder belum ada
			if(mkdir('./files/'.$name, 0755, TRUE)){
				// kalau folder terbuat
				return TRUE;
			}
			else{
				// gagal membuat folder
				return FALSE;
			}
		}
		else{
			// folder sudah ada
			return FALSE;
		}
	}

	private function createFolderAct($name){ // done
		if ($this->createFolder($name)) {
			$data = array("nama_folder"=>$name);
			if($this->ModelFolder->insert($data)){
				return TRUE;
			}
			else{
				return FALSE;
			}
		}
		else{
			return FALSE;
		}
	}

	private function uploadFile($folder){
		$config['upload_path']          = 'files/'.$folder;
		// $config['allowed_types']        = 'pdf|doc|docx|odt|xps|xla|xls|xlsx|ppt|pptx';
		$config['allowed_types']        = 'pdf';
		$this->load->library('upload',$config);
		if ($this->upload->do_upload('form_name')) {
			return TRUE;
		}
		else{
			return FALSE;
		}
	}

	private function uploadFileAct($id,$folder){
		$tag = "";
		if (count($this->input->post('tags'))>0) {
			foreach ($this->input->post('tags') as $key => $value) {
				$tag = $tag."'{$value}',";
				$this->Model_tags->save($value);
			}
		}

		$tagId = array();
		foreach ($this->Model_tags->getIn(rtrim($tag,","))->result() as $key => $value) {
			array_push($tagId, $value->id);
		}		
		if ($this->uploadFile($folder)) {
			$data = array(
				"id_folder"=>$id,
				"nama_file"=>$this->upload->data('raw_name'),
				"type_file"=>$this->upload->data('file_ext'),
				"size_file"=>$this->upload->data('file_size'),
				"active"=>1,
				"desc"=>$this->input->post("desc"),
				"tag"=>implode(",", $tagId)				
			);
			if ($this->ModelFile->insert($data)) {
				return TRUE;
			}
			else{
				return FALSE;
			}
		}
		else{
			return FALSE;
		}
	}

	private function deleteFile($file){
		if (unlink('files/'.$file)) {
			return TRUE;
		}
		else{
			return FALSE;
		}
	}

	private function deleteFileAct($id,$file){
		if ($this->deleteFile($file)) {
			// hapus juga dari database
			if ($this->ModelFile->delete($id)) {
				return TRUE;
			}
			else{
				return FALSE;
			}
		}
		else{
			return FALSE;
		}
	}

	private function renameFile($o,$name){
		if (is_file('files/'.$o)) {
			// kalau folder ada
			if(rename('./files/'.$o,'./files/'.$name)){
				// kalau folder terhapus
				return TRUE;
			}
			else{
				// gagal menghapus folder
				return FALSE;
			}
		}
		else{
			// folder belum ada
			return FALSE;
		}		
	}

	private function renameFileAct($id,$o,$name,$ext,$desc,$tag){
		$tags = "";
		if (count($tag)>0) {
			foreach ($tag as $key => $value) {
				$tags = $tags."'{$value}',";
				$this->Model_tags->save($value);
			}
		}
		$tagId = "";
		if ($tags != "") {
			foreach ($this->Model_tags->getIn(rtrim($tags,","))->result() as $key => $value) {
				$tagId = $tagId."{$value->id},";
			}
		}
		$d = $this->session->userdata("folder_name")."/";
		if ($this->renameFile($d.$o.$ext,$d.$name.$ext)) {
			if ($this->ModelFile->rename($id,$name,$desc,rtrim($tagId,","))) {
				return TRUE;
			}
			else{
				return FALSE;
			}
		}
		else{
			return FALSE;
		}
	}
	public function active($id=NULL,$value=NULL){
		if (count($this->session->userdata()) > 1) {
			if ($this->ModelFile->active($id,$value)) {
				redirect(base_url("index.php/Welcome/Dashboard"));
			}
			else{
				redirect(base_url("index.php/Welcome/Dashboard"));
			}
		}
		else{
			$this->load->view("Template/FormLogin");
		}
	}
	public function Rename(){
		$tag = array();
		if ($this->input->post("tags") != "") {
			$tag = $this->input->post("tags");
		}
		if ($this->renameFileAct(
			$this->input->post("id"),
			$this->input->post("on"),
			str_replace(" ", "_", $this->input->post("nama_file")),
			$this->input->post("ext"),
			$this->input->post("desc"),
			$tag
		)) {
			redirect(base_url("index.php/Welcome/Dashboard"));
		}
		else{
			redirect(base_url("index.php/Welcome/Dashboard"));
		}
	}

	public function ubahPassword(){
		if (count($this->session->userdata()) > 1) {
			if (str_replace(" ", "_", $this->input->post("rPassword")) == str_replace(" ", "_", $this->input->post("nPassword"))) {
				if ($this->Model_welcome->ubahPassword(md5(str_replace(" ", "_", $this->input->post("rPassword"))))) {
					redirect(base_url("index.php/Welcome/Dashboard"));
				}
				else{
					redirect(base_url("index.php/Welcome/Dashboard"));
				}
			}
			else{
				redirect(base_url("index.php/Welcome/Dashboard"));
			}
		}
		else{
			$this->load->view("Template/FormLogin");
		}
	}
	public function ubahUsername(){
		if (count($this->session->userdata()) > 1) {
			//Array ( [username] => coba )
			$data = array(
				"nama"=>str_replace(" ", "_", $this->input->post("username")),
				"user_login"=>str_replace(" ", "_", $this->input->post("userlogin"))
			);
			if ($this->Model_welcome->ubahUsername($data)) {
				$userdata = array(
					"nama_user"=>str_replace(" ", "_", $this->input->post("username")),
					"login_user"=>str_replace(" ", "_", $this->input->post("userlogin"))
				);
				$this->session->set_userdata($userdata);
				redirect(base_url("index.php/Welcome/Dashboard"));
			}
			else{
				redirect(base_url("index.php/Welcome/Dashboard"));
			}
		}
		else{
			$this->load->view("Template/FormLogin");
		}
	}

}
