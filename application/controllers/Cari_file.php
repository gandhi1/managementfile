<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH.'libraries/REST_Controller.php';

class Cari_file extends REST_Controller{
	public function __construct(){
        parent::__construct();
        $this->load->model(array("Model_getfile"));
    }

	function index_get($k = NULL){
		$datas = array();
		if ($k == "") {
			foreach ($this->Model_getfile->getAll()->result() as $key => $value) {
				$d = array(
					'title'=>$value->nama_file,
					'subtitle'=>$value->desc,
					'status'=>'',
					'timestamp'=>$value->create_at,
					'url'=>base_url('files/'.$value->nama_folder.'/'.$value->nama_file.$value->type_file),
					'type'=>'',
					'thumbnail'=>$value->type_file
				);
				array_push($datas, $d);
			}
		}
		else{
			foreach ($this->Model_getfile->cari($k)->result() as $key => $value) {
				$d = array(
					'title'=>$value->nama_file,
					'subtitle'=>$value->desc,
					'status'=>'',
					'timestamp'=>$value->create_at,
					'url'=>base_url('files/'.$value->nama_folder.'/'.$value->nama_file.$value->type_file),
					'type'=>'',
					'thumbnail'=>$value->type_file
				);
				array_push($datas, $d);
			}			
		}
		$res = array(
			'status'=>1,
			'message'=>'sukses',
			'data'=>$datas
		);
		$this->response($res);
	}
}
?>